Adán se casó con Eva, Y con sus pocos ahorros. Se compraron dos chinchorros y alquilaron una cueva. Y a la siguiente semana, ya arreglados sus asuntos, salieron a darle juntos, una vuelta a la manzana. Y fue en aquella ocasión, fue en aquel triste minuto, cuando encontraron el fruto, que causó su perdición.
EVA: ¿Qué fruta es ese color granate? ¿Será tomate? ¿Será mamón?
ADÁN: Ni son naranjas ni son limones.
EVA: ¿Y pimentones?
ADÁN: ¡tampoco son!
EVA: La mata en su ramazón, a la de almendrón imita.
ADÁN: ¿Almendrón? ¡Qué va, mijita! Yo conozco el almendrón! Eva se acerca al manzano, pero al estar junto a él, con un machete en la mano, lo detiene San Miguel.
SAN MIGUEL: Si no queréis que lejos os boten, del jardín oíd estos consejos, que os doy en buen latín. Podéis comer caimito, batata y quimbombó, cambur y cariaquito,¡pero manzana no!
Y el que haga caso omiso, de tal prohibición, saldrá del Paraíso, lo mismo que un tapón.
Se evapora San Miguel y entonces sale una fiera, semejante a la manguera, de una bomba Super-Shell.
MANGUERA: No le hagas caso, mujer, si quieres comer manzanas, no te quedes con las ganas, que nadie lo va a saber.
 Y al probar Eva el sabor, del fruto que tanto ansiaba, se vuelve pájara brava, por no decir lo peor.
 EVA: ¡Quiero joyas y oropeles!, ¡quiero pieles y champán!, quiero viajes por Europa!, ¡Quiero sopa de faisán!, ¡Quiero un novio que se vista!, ¡No un nudista como Adán; 
Aplaude alegre el reptil, Eva baila con un oso Y Adán está más furioso que un loco en ferrocarril. 
Sale Adán junto a la fuente, jugando con un rana, diversión intrascendente, muy propia de un inocente, que no ha comido manzana. 
Y es aquí cuando Eva llega, con un traje tan conciso, que se le ve El Paraíso, por la parte de La Vega.
EVA: Adán, ¿por qué tan callado? Dime, amor, ¿qué te resiente?
ADAN: Que entre tú y esa serpiente, me tienen muy disgustado.
EVA: ¡Pero si todo es en chanza! ¡Y esa culebra es tan mansa, como el caballo y la cebra...!
ADAN: Pero para ser culebra, le has dado mucha confianza. Yo soy tu burla, tu guasa, Y en cambio con la serpiente, te muestras tan complaciente, que ella es quien manda en la casa. ¡Eso es lo triste y lo cruel, de la amistad con culebra, que si uno les da una hebra, se cogen todo el carrete! 
EVA: Bueno, Adán, aquí hay manzana.
ADAN: ¡No quiero!
EVA: ¿Por qué, negrito?
ADAN: Porque no tengo apetito, ni me da mi perra gana!
EVA: Un pedacito ... Sé bueno ... Pruébala ... ¡Sabe a bizcocho!
ADAN: No puedo. Comí topocho y a lo mejor me enveneno.. 
(Furiosa, escupiendo Plomo, Eva coge un arma nueva y antes de que Adán se mueva, se la sacude en el lomo) .
EVA: ¡Vamos, Adán, no más plazos! Aquí tienes dos docenas: ¡Te las comes por las buenas, o te las meto a escobazos!
ADAN: Bueno, sí, voy a comer: pero no arriesgues tu escoba, mira que el palo es caoba y es muy fácil de romper. 
Y arrodillándose allí, como un moderno cristiano, coge la fruta en la mano, se la come y dice así:
ADAN: Por testigo pongo a Dios, de que si comí manzana, la culpa es de esta caimana, pues me puso en tres y dos!
LA VOZ DEL VIEJO: Pues transgredisteís así, mis órdenes oficiales. ¡Amarrad los macundales, y eso es saliendo de aquí!
AUTOR: Y así acaba el astrakán, donde en subidos colores, se les mostró a los lectores, la torta que puso Adán.
